def reverser
  yield.split(" ").map(&:reverse).join(" ")
end

def adder(summand = 1)
  yield + summand
end

def repeater(repeats = 2)
  repeats.times { yield }
end
