def measure(runs = 1)
  start_time = Time.now
  runs.times{ yield }
  end_time = Time.now
  (end_time - start_time) / runs
end
